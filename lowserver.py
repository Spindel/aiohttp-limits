#!/usr/bin/env python3
import asyncio
from aiohttp import web
import random


async def handler(request):
    print(request.app)
    ts = random.uniform(5, 15)
    await asyncio.sleep(ts)
    return web.Response(text=f"{ts}")


async def main():
    server = web.Server(handler)
    runner = web.ServerRunner(server)
    print(server.connections)
    await runner.setup()
    site = web.TCPSite(runner, 'localhost', 8080)
    await site.start()

    print("======= Serving on http://127.0.0.1:8080/ ======")

    # pause here for very long time by serving HTTP requests and
    # waiting for keyboard interruption
    await asyncio.sleep(100*3600)


def entrypoint():
    app = web.Application(
        middlewares = []
    )
    app["semaphore"] = asyncio.Semaphore(value=30)
    app["concurrent"] = {}
    app.add_routes(
        [web.get("/get", hello)])

    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        pass
    loop.close()

if "__name__" == __main__:
    entrypoint()
