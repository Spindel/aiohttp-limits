#!/usr/bin/env python3
import weakref
import asyncio

import random

from aiohttp import web
from aiohttp.web import middleware

import uuid
import time


class Delay:
    """A class representing a timestamp"""
    def __init__(self):
        self.start = time.monotonic()

    @property
    def spent(self):
        now = time.monotonic()
        return now - self.start

    def __hash__(self):
        return hash(self.start)

    def __eq__(self, other):
        return self.start == other.start

    def __lt__(self, other):
        return self.start > other.start

    def __gt__(self, other):
        return self.start < other.start

    def __repr__(self):
        return f"{self.start},{self.spent}"


@middleware
async def requestid_middleware(request, handler):
    """Middleware that adds an Request ID to a request, if missing, and ensures
    it's also there in the respnse"""

    req_id = request.headers.get("X-Request-ID", f"{uuid.uuid4()}")
    request["id"] = req_id
    response = await handler(request)
    response.headers["X-Request-ID"] = request["id"]
    return response


@middleware
async def semaphore_middleware(request, handler):
    """Uses an asyncio.semaphore to limit the amount of requests managed at
    each time. Does not care about latency, and may have priority-inversion
    issues when the next request gets the semaphore.

    To get `LIFO` Behavior, patch Semaphore to have:
            def _wake_up_next(self):
                # use popright instead of popleft.
    """
    async with request.app["semaphore"]:
        resp = await handler(request)
    return resp


def timelimit_middleware_factory(duration, close_socket=True):
    """Generate a middleware that discards new connetions if the currently
    processed ones have taken more than `duration` amount of seconds.
        close_socket   - Close the socket rather than 503, this permits a
        frontend load balancer to retry the connection
        duration    - Whole (or fractional) of seconds
    """

    @middleware
    async def timlimit_middleware(request, handler):
        """Rejects incoming requests if the currently processed requests are
        taking more than a set amount of time.
        Requires there to be an `id` field on the request."""

        req_id = request["id"]
        request.app["concurrent"][req_id] = Delay()

        requests = list(request.app["concurrent"].values())
        max_delay, working_requests = max(requests), len(requests)
        del requests

        print("current=", working_requests, "max_delay=", max_delay.spent)

        if max_delay.spent > duration:
            # We're lagging behind what we can process.
            print("Hang on, we're lagging behind")

            if close_socket:
                # Close the TCP connection and leave it like that.
                # This allows an upstream proxy to just retry the transmission
                request.transport.close()
            else:
                # Be kind to the client application, return a normal error code
                resp = web.Response(text='No', status=503)
        else:
            resp = await handler(request)
        delay = request.app["concurrent"].pop(req_id)

        return resp

    return timlimit_middleware



async def hello(request):
    ts = random.uniform(5, 15)
    await asyncio.sleep(ts)
    return web.Response(text=f"{ts}")


async def main(loop):
    server = web.Server(hello)
    await loop.create_server(server, "127.0.0.1", 8080)
    print("Serving on http://127.0.0.1:8080")
    await asyncio.sleep(100*3600)


def entrypoint():
    app = web.Application(
        middlewares=[
            requestid_middleware,
            timelimit_middleware_factory(2),
            semaphore_middleware,
    ])
    app["semaphore"] = asyncio.Semaphore(value=30)
    app["concurrent"] = {}
    app.add_routes([web.get("/get", hello)])

    web.run_app(app, host="127.0.0.1", port=8080)

if "__main__" == __name__:
    entrypoint()
