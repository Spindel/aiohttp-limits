#!/usr/bin/env python3
import aiohttp
import asyncio

import time
async def fetchone(session, num):
    async with session.get(f"http://localhost:8080/get?{num}") as resp:
        try:
            data = await resp.text()
        except:
            return
        print(num, data,  resp.status)
        return num, resp.status

async def run(loop):
    async with aiohttp.ClientSession() as session:
        futs = (fetchone(session, x) for x in range(100))
        await asyncio.gather(*futs)

def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        run(loop)
    )

if __name__ == "__main__":
    main()
