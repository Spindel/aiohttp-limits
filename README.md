# aiohttp-limits


To test:
 - in one terminal, run ./server.py
 - In _TWO_ other terminals, run ./client.py


The first client will see all it's requests go through the `time limit`
middleware as `OK`, but may be blocked by the semaphore middleware.

The second set of clients will all fail, because the `time limit` middleware
has decided that it's currently processing too many, too slowly.


Open questions:
- A better data structure for the current `processing` items with O(n) lookup
  cost from _both_ directions would be nice.

  
  Ideas:
	Custom wrapper around `dict` that keeps track of max value on insertion?
	
	SortedDict?
	http://www.grantjenks.com/docs/sortedcontainers/sorteddict.html#sortedcontainers.SortedValuesView


